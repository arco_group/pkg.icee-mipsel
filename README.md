This is an OpenWrt package. To compile, please install the proper **OpenWrt buildroot** and then run:

    $ make package

This package has been tested using the following toolchains:

* `carambola-toolchain`
* `yun-toolchain`

both are Debian packages available on:

    deb http://babel.esi.uclm.es/arco sid main

